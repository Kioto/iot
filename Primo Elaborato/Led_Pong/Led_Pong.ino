/* Project #1 - Led Pong :

    Createdy by:
    - Rispoli Luca
    - Lavista Andrea

    The software might not work as expected without further changes to the TimerOne.h library, the problem has been reported on the course's forum.

*/
#include "TimerOne.h"

#define T1_PIN 2
#define T2_PIN 3
#define T3_PIN 4
#define LED_FADE_PIN 11
#define LED1_PIN 8
#define LED2_PIN 9
#define LED3_PIN 10
#define POT_PIN A0
#define PLAYER1 1
#define PLAYER2 2
#define NOBODY -1
#define LIGHT_SPEED 1000000
#define REACTION_TIME 1000000
#define FADE_TIME 20000
#define FLASH_TIME 200000
#define FLASH_INTERVAL 500
#define DEBOUNCE_INTERVAL 200


boolean lightOn;
boolean gameOver;
int speed;
long reactionTime;
int currentTurn;
int direction;
int started;
int brightness;
int fadeAmount;
int currIntensity;
int winner;
/* The total number of ball exchanges*/
int exchanges;
/* Variables for debouncing */
int bouncingTime1;
int bouncingTime2;


void setup() {
  pinMode(LED_FADE_PIN, OUTPUT);
  pinMode(LED1_PIN, OUTPUT);
  pinMode(LED2_PIN, OUTPUT);
  pinMode(LED3_PIN, OUTPUT);
  randomSeed(analogRead(0));
  gameOver = false;
  reactionTime = REACTION_TIME;
  fadeAmount = 5;
  bouncingTime1 = 0;
  bouncingTime2 = 0;
  exchanges = 0;
  Timer1.attachInterrupt(fade);
  Timer1.initialize(FADE_TIME);
  Serial.begin(9600);
  Serial.println("Welcome to Led Pong. Press Key T3 to Start!");
}

/* Called when one player loses the game */
void endGame(int loser) {
  if (!gameOver) {
    winner = loser == PLAYER1 ? PLAYER2 : PLAYER1;
    gameOver = true;
    digitalWrite(LED1_PIN, LOW);
    digitalWrite(LED2_PIN, LOW);
    digitalWrite(LED3_PIN, LOW);
    Timer1.detachInterrupt();
    Serial.println("Game Over - The Winner is player " + String(winner) + " after " + String(exchanges) + " Shots");
  }
}

/* Manages the fading of the led at the start of the game */
void fade() {
  analogWrite(LED_FADE_PIN, currIntensity);
  currIntensity = currIntensity + fadeAmount;
  if (currIntensity == 0 || currIntensity == 255) {
    fadeAmount = -fadeAmount ;
  }
}

/* Manages the flashing of the led at the end of the game */
void flash() {
  int ledWinner = winner == PLAYER1 ? LED1_PIN : LED3_PIN;
  if (!lightOn) {
    digitalWrite(ledWinner, HIGH);
  } else {
    digitalWrite(ledWinner, LOW);
  }
  lightOn = !lightOn;
}



void loop() {
  /* Before the game */
  if (!started) {
    started = digitalRead(T3_PIN);
    if (started) {
      Serial.println("Go!");
      digitalWrite(LED_FADE_PIN, LOW);
      digitalWrite(LED2_PIN, HIGH);
      /* Speed taken from the potentiometer*/
      speed = map(analogRead(POT_PIN), 0, 1023, 1, 5);
      direction = random(2);
      attachInterrupt(digitalPinToInterrupt(T1_PIN), button1Pressed, RISING);
      attachInterrupt(digitalPinToInterrupt(T2_PIN), button2Pressed, RISING);
      configureTimerInterrupt(moveBall, LIGHT_SPEED / speed);
    }
  }
  /* After the game */
  if (gameOver) {
    configureTimerInterrupt(flash, FLASH_TIME);
    delay(2000);
    exit(0);
  }
}

/* Function to manage the timer used for calling interrupts. */
void configureTimerInterrupt(void (*handler)(void), long interval) {
  Timer1.stop();
  Timer1.detachInterrupt();
  Timer1.attachInterrupt(handler, interval);
  Timer1.restart();
  TCNT1 = 1;  //To avoid early interrupt calls due to overflow

}

/* Moves the ball from the center to the direction of the next player, it also manages the reaction time. */
void moveBall() {
  if (direction == PLAYER1) {
    digitalWrite(LED1_PIN, HIGH);
  } else {
    digitalWrite(LED3_PIN, HIGH);
  }
  digitalWrite(LED2_PIN, LOW);
  currentTurn = (direction == PLAYER1) ? PLAYER1 : PLAYER2;
  configureTimerInterrupt(endTurn, reactionTime);
  reactionTime -= reactionTime / 8;
}

/* Called whenever Player 1 presses the button, calls caused by bouncing are ignored. */
void button1Pressed() {
  if ((millis() - bouncingTime1) > DEBOUNCE_INTERVAL && !gameOver) {
    manageTurn(PLAYER1);
    bouncingTime1 = millis();
  }
}

/* Called whenever Player 2 presses the button, calls caused by bouncing are ignored. */
void button2Pressed() {
  if ((millis() - bouncingTime2) > DEBOUNCE_INTERVAL && !gameOver) {
    manageTurn(PLAYER2);
    bouncingTime2 = millis();
  }
}
/* Called at every valid player move, switches turn and starts the timer that brings the ball to the center, if the player didn't score, ends the game. */
void manageTurn(int turn) {
  if (currentTurn == turn) {
    currentTurn = NOBODY;
    configureTimerInterrupt(moveBall, LIGHT_SPEED / speed);
    exchanges++;
    turnBall();
  } else {
    endGame(turn);
  }
}
/* Called when the ball reaches the center, computes the new direction and manages the lights accordingly.*/
void turnBall() {
  direction = (direction == PLAYER1) ? PLAYER2 : PLAYER1;
  digitalWrite(LED2_PIN, HIGH);
  digitalWrite(LED1_PIN, LOW);
  digitalWrite(LED3_PIN, LOW);
}

/* Called when one player fails to hit the ball within the reaction time treshold. */
void endTurn() {
  endGame(currentTurn);
}
