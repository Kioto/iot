import coffe_machine_gui.View;
import coffe_machine_gui.ViewImpl;
import serial_communication.CommChannel;
import serial_communication.SerialCommChannel;

public class Main {
	
	static private String MAINTENANCE_MSG = "No more coffee. Waiting for recharge";
	static private String SUGAR_LEVEL_CHANGED_MSG = "Sugar amount changed: ";
	static private String COFEE_REFILLED_MSG = "Coffee refilled: ";
	
	/**
	 * The main class, its purpose is to initialize the Graphical user interface and
	 * handle communications with Arduino, the GUI is managed by a separated thread.
	 * 
	 * 
	 * @param args
	 *            The port connected to arduino. E.g "COM3"
	 */
	public static void main(String[] args) throws Exception {

		CommChannel channel = new SerialCommChannel(args[0], 9600);
		View view = new ViewImpl(channel);
		view.start();
		System.out.println("Waiting Arduino for rebooting...");
		Thread.sleep(4000);
		System.out.println("Ready.");
		Thread guiInstance = new Thread(new Runnable() {
			public void run() {
				String receivedMessage;
				while (!view.isClosed()) {
					view.update();

					try {
						receivedMessage = channel.receiveMsg();
						//System.out.println(receivedMessage);
						if (receivedMessage.contains(MAINTENANCE_MSG)) {
							view.setMaintenanceState(true);
							view.setCurrentMessage(receivedMessage);
						} else if (receivedMessage.contains(COFEE_REFILLED_MSG)) {
							view.setMaintenanceState(false);
							view.setCurrentMessage(receivedMessage);
						} else if (receivedMessage.contains(SUGAR_LEVEL_CHANGED_MSG)) {
							view.setCurrentSugar(receivedMessage);
						} else {
							view.setCurrentMessage(receivedMessage);
						}
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		guiInstance.start();
	}

}
