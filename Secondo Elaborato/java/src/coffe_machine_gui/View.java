package coffe_machine_gui;

/**
 * A simple view interface that manages the coffee machine GUI, it is also responsible for sending messages to arduino
 * 
 *
 */

public interface View {
    
    /**
     * Initializes the GUI
     */
    void start();
    
    /**
     * Update the interface with the new data
     */
    void update();
    
    /**
     * Getter for Window state
     * @return A boolean relative to the Jframe State (Closed, Open)
     */
    boolean isClosed();
    
    /**
     * Set a new message to be displayed
     * @param message The message to be displayed.
     */
    void setCurrentMessage(String message);
    
    /**
     * Set the current sugar value
     * @param sugar A string containing the sugar value
     */
    void setCurrentSugar(String sugarMessage);
    
    /**
     * Set the maintenance button state
     * @param state A boolean relative to the new button state
     */
    void setMaintenanceState(boolean state);
    

}
