package coffe_machine_gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import serial_communication.CommChannel;

public class ViewImpl implements View {

	private static double DEFAULT_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	private static double DEFAULT_HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
	public static int MAX_SUGAR_VALUE = 6;
	public static int MIN_SUGAR_VALUE = 0;
	private static double DEFAULT_WIDTH_SCALE = 3;
	private static double DEFAULT_HEIGHT_SCALE = 7;
	private CommChannel channel;
	private JFrame frame;
	private JPanel mainPanel;
	private TextField coffeAmount;
	private JLabel textAreaLabel;
	private JButton chargeCoffe;
	private JLabel statusLabel;
	private volatile boolean isClosed = false;
	private String currentMessage;
	private JLabel sugarLabel;
	private String sugarMessage;
	

	public ViewImpl(CommChannel channel) {
		this.initialize();
		this.customizeItems();
		this.channel = channel;
	}

	void initialize() {
		frame = new JFrame();
		mainPanel = new JPanel();
		coffeAmount = new TextField();
		textAreaLabel = new JLabel("Please Insert amount of coffee to refill:");
		chargeCoffe = new JButton("ChargeCoffe");
		statusLabel = new JLabel("Last message:");
		this.setMaintenanceState(false);
		this.sugarLabel = new JLabel();
		
	}

	/**
	 * Update properties of swing elements.
	 */
	private void customizeItems() {
		frame.setTitle("Coffe Machine");
		frame.setSize((int) (DEFAULT_WIDTH / DEFAULT_WIDTH_SCALE), (int) (DEFAULT_HEIGHT / DEFAULT_HEIGHT_SCALE));
		coffeAmount.setColumns(20);
		chargeCoffe.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					Integer.parseInt(coffeAmount.getText());

				} catch (Exception ex) {
					System.out.println("Perfavore, inserisci un numero!");
					return;
				}
				channel.sendMsg(coffeAmount.getText());
			}
		});
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				isClosed = true;
			}
		});
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		coffeAmount.setMaximumSize(new Dimension(Integer.MAX_VALUE, 20));
		mainPanel.add(textAreaLabel);
		mainPanel.add(coffeAmount);
		mainPanel.add(chargeCoffe);
		mainPanel.add(statusLabel);
		mainPanel.add(sugarLabel);
		frame.add(mainPanel);

	}

	public void update() {
		if (currentMessage != null) {
			this.statusLabel.setText("Last message: " + currentMessage);
		}
		this.sugarLabel.setText(sugarMessage);
	}

	public boolean isClosed() {
		return isClosed;
	}

	public void start() {
		frame.setVisible(true);
	}

	public void setCurrentMessage(final String message) {
		this.currentMessage = message;
	}

	@Override
	public void setMaintenanceState(final boolean state) {
		this.chargeCoffe.setEnabled(state);

	}

	@Override
	public void setCurrentSugar(final String sugarMessage) {
		this.sugarMessage = sugarMessage;
	}

}
