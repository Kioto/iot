/* An Header File containing configuration data for the Machine */

#ifndef __CONFIGURATION__
#define __CONFIGURATION__

/* Coffee machine configuration */
#define DIST1 30 // Engagement Distance : 30 Centimeters
#define DIST2 10	//DIST2 (Take coffee) = 0.1 m = 10 cm
#define DT1 1000 // Min. Engagement Time: 1 Second
#define DT3 3000	//DT3 (Coffee making process duration) = 3 s = 3000 ms
#define DT4 5000	//DT4 (Max time to remove coffee) = 5 s = 5000 ms
#define DT2A 5000 // Max Time with no engagement: 5 Seconds
#define DT2B 5000 // Max Time with no presence: 5 Seconds
#define SUGAR_AMOUNT_MIN 1
#define SUGAR_AMOUNT_MAX 6
#define SUGAR_AMOUNT_DEFAULT 3
#define NMAXCOFFEE 5 // Max Number of coffe in one charge

/* Arduino pin configuration */
#define NUM_LEDS 3
#define POT_PIN A0
#define LED1_PIN 8
#define LED2_PIN 9
#define LED3_PIN 10
#define SNR_ECHO_PIN 6
#define SNR_TRIG_PIN 7
#define PIR_PIN 3
#define BTN_PIN 2

/* All the states allowed for the coffee machine */
typedef enum {STAND_BY, ON, READY, MAINTENANCE, MAKING_COFFEE, COFFEE_DONE} Status;  

#endif
