/* Implementation of MachineControl.h , the task that controls the coffe machine and its transitions from STAND_BY state to READY. */
#include "MachineControl.h"

MachineControl::MachineControl(PirSensor* pir, SonarSensor* sonar){
  this->pir = pir;
  this->sonar = sonar;
} 

/* ISR needed to wake up Arduino from enery saving mode */
static void wake(){
  sleep_disable();
} 

void MachineControl::init(int period){
  Task::init(period);
}


void MachineControl::tick(){
  switch (MachineStatus::getInstance()->getMachineState()){
    case STAND_BY: {
    //EIFR = bit (INTF0);
	  start_sleep();
    detachInterrupt(digitalPinToInterrupt(PIR_PIN));
      if(pir->isDetected()){
        MachineStatus::getInstance()->setMachineState(ON);
#ifdef __DEBUG__
    debugInformations();
#endif
      }
      }
      break;
    case ON: {
    if(pir->isDetected() && sonar->getDistance() <= DIST1){
      idleTime = 0;
      engagementTime += myPeriod;
      if(engagementTime >= DT1){
        engagementTime = 0;
        MachineStatus::getInstance()->setMachineState(READY);
        Serial.println("Welcome!");
      }
    }
    else if (!pir->isDetected()){
      engagementTime = 0;
      idleTime += myPeriod;
      if(idleTime >= DT2B){
        idleTime = 0;
        MachineStatus::getInstance()->setMachineState(STAND_BY);
#ifdef __DEBUG__
        debugInformations();
#endif
      }     
    }
    
      
      break;
    }
    case READY: {
    if(!pir->isDetected() || sonar->getDistance() >= DIST1){
      idleTime += myPeriod;
      if(idleTime >= DT2A){
        idleTime = 0;
        MachineStatus::getInstance()->setMachineState(ON);
#ifdef __DEBUG__
        debugInformations();
#endif
      }
    }
    else{
      idleTime = 0;
    }
      break;
    } 
  
}
}

void MachineControl::start_sleep(){
  // Disable interrupts when setting up sleep mode.
  noInterrupts ();
  // Clear interrupt flag before attaching interrupts.
  EIFR = bit (INTF0);
  attachInterrupt(digitalPinToInterrupt(PIR_PIN), wake, HIGH);
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_enable();
  // Enable interrupts to allow waking up.
  interrupts (); 
	sleep_cpu();
  
}

void MachineControl::debugInformations(){
  Serial.println("Current State is "+ String(MachineStatus::getInstance()->getMachineState()) + " The Pir Sensor reads " + String(pir->isDetected()) + " The Sonar Sensor reads " + String(sonar->getDistance()));
  Serial.flush();
}
