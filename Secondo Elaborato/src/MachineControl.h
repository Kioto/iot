/* An interface modelling the coffe machine control interface, it manages the main states of the machine. */

#ifndef __MACHINECONTROL__
#define __MACHINECONTROL__
#include "Task.h"
#include "SonarSensor.h"
#include "PirSensor.h"
#include "Configuration.h"
#include "MachineStatus.h"
#include "Arduino.h"
#include <avr/power.h>
#include <avr/sleep.h>

class MachineControl: public Task{
  
public: 
  MachineControl(PirSensor* pir, SonarSensor* sonar);
  void init(int period); 
  void tick();
  
  
private:
  PirSensor* pir;
  SonarSensor* sonar;
  int engagementTime;
  int idleTime;
  void debugInformations();
  void start_sleep();


};
  


#endif
