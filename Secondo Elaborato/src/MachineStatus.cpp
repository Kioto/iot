/*  */
#include "Arduino.h"
#include "Configuration.h"
#include "MachineStatus.h"

MachineStatus* MachineStatus::machineStatus = new MachineStatus();

MachineStatus::MachineStatus(){  
	dosesAmount = NMAXCOFFEE;
}

bool MachineStatus::coffeAvailable(){
	return dosesAmount ? true : false;
}
void MachineStatus::consumeCoffe(){
	if(coffeAvailable()){
		dosesAmount--;
	}
}

int MachineStatus::getDosesAmount(){
	return dosesAmount;
}

Status MachineStatus::getMachineState(){
	return currentState;
}
	
void MachineStatus::setMachineState(Status newState){
	currentState = newState;
}

void MachineStatus::addDoses(int nc) {
	dosesAmount += nc; 
}

MachineStatus* MachineStatus::getInstance() {
	return machineStatus;
}
