/* An interface modelling an object containing status information about the coffee machine.*/
#ifndef __MACHINESTATUS__
#define __MACHINESTATUS__
#include "Configuration.h"

class MachineStatus{

public: 
  static MachineStatus* getInstance();
  void consumeCoffe();
  int getDosesAmount();
  Status getMachineState();
  void setMachineState(Status newState);
  bool coffeAvailable();
  void addDoses(int nc);
  MachineStatus();
  
private:
  int dosesAmount;
  Status currentState;
  static MachineStatus* machineStatus;

};

#endif
