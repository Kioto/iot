#include "Arduino.h"
#include "MaintenanceTask.h"
#include "MsgService.h"

MaintenanceTask::MaintenanceTask(){
  maintenanceActive = false;
} 

void MaintenanceTask::init(int period){
  Task::init(period);
}

void MaintenanceTask::tick(){
  
  switch (MachineStatus::getInstance()->getMachineState()){
    case READY:
      /* check coffee doses absence */
      if(!MachineStatus::getInstance()->coffeAvailable()) {
        MachineStatus::getInstance()->setMachineState(MAINTENANCE);
      }
      break;
    case MAINTENANCE:
      /* nnotify coffee doses absence */
      if(!maintenanceActive) {
        Serial.println("No more coffee. Waiting for recharge"); 
        maintenanceActive = true;
      }
      /* wait for coffee doses loading */
      if(MsgService.isMsgAvailable()) {
        String msg = MsgService.receiveMsg()->getContent();
        char str[10];
        msg.toCharArray(str, 10);
        int nc = atoi(str);
		if(nc > NMAXCOFFEE){
			Serial.println("Please insert less than "+ String(NMAXCOFFEE) + " Coffee");
			return;
		}
        MachineStatus::getInstance()->addDoses(nc);
        Serial.println("Coffee refilled: " + String(MachineStatus::getInstance()->getDosesAmount()));
        MachineStatus::getInstance()->setMachineState(STAND_BY);
        maintenanceActive = false;
     }
     break;    
  }
}
