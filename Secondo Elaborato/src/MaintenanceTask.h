/* An interface modelling the coffe machine maintenance task. */
#ifndef __MAINTENANCE__
#define __MAINTENANCE__
#include "Task.h"
#include "Configuration.h"
#include "MachineStatus.h"

class MaintenanceTask: public Task{
  
public: 
  MaintenanceTask();
  void init(int period); 
  void tick();

private:
  int maintenanceActive;
  
};
  


#endif

