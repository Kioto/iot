#include "MakingCoffeeTask.h"
#include "Arduino.h"

MakingCoffeeTask::MakingCoffeeTask(Light** leds, int numLeds, Button* btn, Potentiometer* pot, SonarSensor* snr){
  this->leds = leds;
  this->numLeds = numLeds;
  this->btn = btn;
  this->pot = pot;
  this->snr = snr;
}
  
void MakingCoffeeTask::init(int period){
  Task::init(period);
  progress = 0;
  sugar = pot->getValue();
}
  
void MakingCoffeeTask::tick(){
  Serial.println(snr->getDistance());
  switch(MachineStatus::getInstance()->getMachineState()) {
    int potValue;
    case READY: 
      /* read sugar level */
      potValue = map(analogRead(pot->getValue()), 0, 1023, SUGAR_AMOUNT_MIN, SUGAR_AMOUNT_MAX);
      if(potValue != sugar) {
        sugar = potValue;
        Serial.println("Sugar level changed: " + String(sugar));
      }
      #ifdef __DEBUG__
          Serial.println("Dosi attuali: " + String(MachineStatus::getInstance()->getDosesAmount()));
        #endif 
      /* coffee request */
      if(btn->isPressed() && MachineStatus::getInstance()->coffeAvailable()) {
        MachineStatus::getInstance()->setMachineState(MAKING_COFFEE);
        timer = 0;
        Serial.println("Making a coffee");
      }
      break;
    case MAKING_COFFEE:
      /* switch on leds progressively */
      progress = (progress) % (numLeds - 1);
      leds[progress]->switchOn();
      timer += myPeriod;
      if(timer > (DT3 / (numLeds-1))) {
        timer = 0;
        progress++;
        if(progress == numLeds - 1) {
          leds[progress]->switchOn();
          MachineStatus::getInstance()->setMachineState(COFFEE_DONE);
          Serial.println("The coffee is ready");
        }
      }
      break;
    case COFFEE_DONE:
      /* wait user take the coffee or DT4 seconds */
      timer += myPeriod;
      if(snr->getDistance() < DIST2 || timer > DT4) {
        MachineStatus::getInstance()->consumeCoffe();
        MachineStatus::getInstance()->setMachineState(READY);
        for (int i = 0; i < numLeds; i++){
          leds[i]->switchOff(); 
        }
      }
      #ifdef __DEBUG__
        Serial.println("Dosi attuali: " + String(MachineStatus::getIstance()->getDosesAmount()));
      #endif 
      break;

      
  }
}
