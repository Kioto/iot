#ifndef __MAKINGCOFFEETASK__
#define __MAKINGCOFFEETASK__

#include "Task.h"
#include "Configuration.h"
#include "Led.h"
#include "ButtonImpl.h"
#include "PotentiometerImpl.h"
#include "SonarSensor.h"
#include "MachineStatus.h"

class MakingCoffeeTask: public Task {

  /*int led_pin[NUM_LED], btn_pin, pot_pin;*/
  Light** leds;
  int numLeds;
  Button* btn;
  Potentiometer* pot;
  SonarSensor* snr;

  int state;
  int sugar, progress;
  unsigned long timer;

public:

  MakingCoffeeTask(Light** leds, int numLeds, Button* btn, Potentiometer* pot, SonarSensor* snr);  
  void init(int period);  
  void tick();
};

#endif
