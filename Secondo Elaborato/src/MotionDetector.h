#ifndef __MotionDetector__
#define __MotionDetector__

class MotionDetector {

public:
  virtual bool isDetected() = 0;
  
};


#endif

