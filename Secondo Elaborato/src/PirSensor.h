/* A Basic interface modelling a pir IR Sensor */

#ifndef __PIRSENSOR__
#define __PIRSENSOR__
#include "MotionDetector.h"

class PirSensor{
	
	
public: 
	PirSensor(int pin);
	bool isDetected();
	
private:
  int pin;
  
};
	


#endif
