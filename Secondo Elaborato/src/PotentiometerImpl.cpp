#include "PotentiometerImpl.h"
#include "Arduino.h"

PotentiometerImpl::PotentiometerImpl(int pin){
  this->pin = pin;
  pinMode(pin, INPUT);     
} 
  
int PotentiometerImpl::getValue(){
  return analogRead(pin);
}
