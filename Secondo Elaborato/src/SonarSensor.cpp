/* Implementation of SonarSensor Interface, values caused by noise are filtered according to the average value . */

#include "Arduino.h"
#include "SonarSensor.h"
#define MAX_DELAY 3000  // Max interval available to search for a valid reading, more time means more precise and slower readings.
#define TO_CENTIMETER 100


SonarSensor::SonarSensor(int echoPin, int trigPin){
  this->echoPin = echoPin;
  this->trigPin = trigPin;
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);   
  oldValue = this->getReading();
} 
  
int SonarSensor::getDistance(){
	newValue = this->getReading();
	int beginTime = micros();
	/* Keep reading until two matching values are found, or until the time has expired */
	while(newValue!=oldValue && micros() > beginTime + MAX_DELAY){
		oldValue = newValue;
		newValue = this->getReading();
	}
	return newValue;
}

int SonarSensor::getReading(){
	/* Send Impulse */
    digitalWrite(trigPin,LOW);
    delayMicroseconds(3);
    digitalWrite(trigPin,HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin,LOW); 
	/* Receive Echo */
    float tUS = pulseIn(echoPin, HIGH);
    float t = tUS / 1000.0 / 1000.0 / 2;
    int d = t*vs*TO_CENTIMETER;
	return d;
}

/* Alternative Method: running average. */
/*bool SonarSensor::checkValue(int value){
	total += value;
	numReadings++;
	int average = total/numReadings;
	return value<(average+MAX_DEVIATION);
	
}*/
