/* A Basic interface modelling a sonar Sensor */
#ifndef __SONARSENSOR__
#define __SONARSENSOR__
#include "DistanceDetector.h"

class SonarSensor{
	
public: 
	SonarSensor(int echoPin, int trigPin);
	int getDistance();
	
private:
	int getReading();
	int echoPin;
	int trigPin;
	int oldValue;
	int newValue;
	const float vs = 331.5 + 0.6*20;
  
};

#endif
