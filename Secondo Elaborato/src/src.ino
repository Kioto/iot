/* Second Assignment : Coffe Machine.
   Made by: Rispoli Luca, Andrea Lavista
   Please do see the attached documentation for further informations.
*/
#include "Scheduler.h"
#include "MakingCoffeeTask.h"
#include "MachineControl.h"
#include "MaintenanceTask.h"
#include "Configuration.h"
Scheduler sched;

void setup() {
  Serial.begin(9600);
  sched.init(100);

  /* variable shared by the tasks */
  MachineStatus::getInstance()->setMachineState(STAND_BY);

  /* declaration and initialization of components */
  Light** leds = (Light**) malloc (sizeof(Led*) * NUM_LEDS);
  if(leds != NULL) {
    leds[0] = new Led(LED1_PIN);
    leds[1] = new Led(LED2_PIN);
    leds[2] = new Led(LED3_PIN);
  } else {
    Serial.println("Malloc failed. Abort process.");
    exit(1);
  }
  Button* btn = new ButtonImpl(BTN_PIN);
  Potentiometer* pot = new PotentiometerImpl(POT_PIN);
  SonarSensor* snr = new SonarSensor(SNR_ECHO_PIN, SNR_TRIG_PIN);
  PirSensor* pir = new PirSensor(PIR_PIN);

  /* declaration and initialization of tasks */
  Task* initialize = new MachineControl(pir, snr);
  initialize->init(100);
  Task* maintenanceCoffee = new MaintenanceTask();
  maintenanceCoffee->init(100);
  Task* makingCoffee = new MakingCoffeeTask(leds, NUM_LEDS, btn, pot, snr);
  makingCoffee->init(200);

  sched.addTask(initialize);  
  sched.addTask(maintenanceCoffee); 
  sched.addTask(makingCoffee);  
  
}

void loop() {
  sched.run();
}
