URL = "http://02f677b4.ngrok.io";

readingsURL = URL + "/api/humidity";
statusURL = URL + "/api/status";
historyURL = URL + "/api/history";
signalURL = URL + "/api/signals";

$(document).ready(function() {
	httpGetAsyncRead(readingsURL,logReadings);
	httpGetAsyncStat(statusURL,logStatus);
	httpGetAsyncHistory(historyURL,logHistory);
	httpGetAsyncHistory(signalURL,logSignal);

	//Update The Readings every 10 seconds
    setInterval( function() {
		httpGetAsyncRead(readingsURL,logReadings);
	},10000);
	
	//Update the Status every 3 seconds .
	setInterval( function() {
		httpGetAsyncStat(statusURL,logStatus);
	},3000);
	
	//Update The History every 20 seconds
	setInterval( function() {
		httpGetAsyncHistory(historyURL,logHistory);
	},20000);
	
	//Update Signals every 10 seconds
	setInterval( function() {
		httpGetAsyncSignal(signalURL,logSignal);
	},10000);
	
 });
 
 
 function httpGetAsyncRead(theUrl, callback){
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.onreadystatechange = function() { 
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
				callback(xmlHttp.responseText);
			}
		xmlHttp.open("GET", theUrl, true); // true for asynchronous 
		xmlHttp.send(null);
}

 function httpGetAsyncSignal(theUrl, callback){
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.onreadystatechange = function() { 
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
				callback(xmlHttp.responseText);
			}
		xmlHttp.open("GET", theUrl, true); // true for asynchronous 
		xmlHttp.send(null);
}

function httpGetAsyncHistory(theUrl,callback){
	var xmlHttp = new XMLHttpRequest();
		xmlHttp.onreadystatechange = function() { 
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
				callback(xmlHttp.responseText);
			}
		xmlHttp.open("GET", theUrl, true); // true for asynchronous 
		xmlHttp.send(null);
}
function httpGetAsyncStat(theUrl, callback){
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.onreadystatechange = function() { 
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
				callback(xmlHttp.responseText);
			}
		xmlHttp.open("GET", theUrl, true); // true for asynchronous 
		xmlHttp.send(null);
}

function logReadings(text){
	//Create and Populate table
	readings = JSON.parse(text);
	// If it's empty, do nothing
	if(text != "[]"){
	$("#rilevamenti").html(json2table(readings,""));
	}
}

function logSignal(text){
	readings = JSON.parse(text);
	// If it's empty, do nothing
	if(text != "[]"){
	$("#segnalazioni").html(json2table(readings,""));
	}
	
}

function logHistory(text){
	readings = JSON.parse(text);
	// If it's empty, do nothing
	if(text != "[]"){
	$("#irrigazioni").html(json2table(readings,""));
	}
}

function logStatus(text){
	reply = JSON.parse(text);
	if(reply.status == "Off"){
		$("#ap").html("<b>Stato Valvola: </b>OFF");
		$("#st").html("");
	}else{
		$("#ap").html("<b>Stato Valvola: </b>ON");
		$("#st").html("<b>Intensità Irrigazione: </b> "+ reply.status);
	}
	
}

function json2table(json, classes) {
  var cols = Object.keys(json[0]);
  
  var headerRow = '';
  var bodyRows = '';
  
  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  cols.map(function(col) {
    headerRow += '<th>' + capitalizeFirstLetter(col) + '</th>';
  });

  json.map(function(row) {
    bodyRows += '<tr>';

    cols.map(function(colName) {
      bodyRows += '<td>' + row[colName] + '</td>';
    })

    bodyRows += '</tr>';
  });

  return '<table border = 1 style = "font-size:150%" class="' +
         classes +
         '"><thead><tr>' +
         headerRow +
         '</tr></thead><tbody>' +
         bodyRows +
         '</tbody></table>';
}