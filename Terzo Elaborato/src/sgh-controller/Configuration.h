/* An Header File containing configuration data for the GreenHouse */

#ifndef __CONFIGURATION__
#define __CONFIGURATION__

/* GreenHouse machine configuration */
#define DIST 30
#define MICROSEC_IN_MILLISEC 1000
/* Levels of irrigation, Liters per minute*/

#define P_MIN 30
#define P_MED 60
#define P_MAX 90

/* Arduino pin configuration */
#define LED1_PIN 8
#define LED2_PIN 5
#define LEDM_PIN 10
#define SNR_ECHO_PIN 6
#define SNR_TRIG_PIN 7
#define BT_TX 13
#define BT_RX 12
#define SERVO_MOTOR_PIN 4
#define MAX_OPENING 2250
#define MIN_OPENING 750

/* Servo Configuration */
#define MAX_OPENING 2250
#define MIN_OPENING 750
#endif
