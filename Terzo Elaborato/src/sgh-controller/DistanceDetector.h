#ifndef __DistanceDetector__
#define __DistanceDetector__

class DistanceDetector {

public:
  virtual int getDistance() = 0;
  
};


#endif

