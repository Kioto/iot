#include "Arduino.h"
#include "IrrigationStatus.h"
#include "Configuration.h"

IrrigationStatus* irrigationStatus = new IrrigationStatus();

IrrigationStatus::IrrigationStatus(){
	on = false;
}

void IrrigationStatus::switchOn() {
  on = true;
}

void IrrigationStatus::switchOff() {
  on = false;
}

bool IrrigationStatus::isOn() {
  return on;
}

void IrrigationStatus::setIntensity(int i) {
  intensity = i;
}

int IrrigationStatus::getIntensity() {
  return intensity;
}

IrrigationStatus* IrrigationStatus::getInstance() {
	return irrigationStatus;
}
