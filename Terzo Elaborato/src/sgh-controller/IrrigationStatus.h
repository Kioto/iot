/* An interface modelling an object containing information about the status of irrigaton (on/off).*/
#ifndef __IRRIGATIONSTATUS__
#define __IRRIGATIONSTATUS__
#include "Configuration.h"

class IrrigationStatus {

public: 
  static IrrigationStatus* getInstance();
  void switchOn();
  void switchOff();
  bool isOn();
  void setIntensity(int i);
  int getIntensity();
  IrrigationStatus();
  
private:
  bool on;
  int intensity;
};

#endif
