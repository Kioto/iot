#include "IrrigationTask.h"


IrrigationTask::IrrigationTask(LightExt* led, Motor* servo){
  this->led = led;
  this->servo = servo;
  led->switchOn();
}
  
void IrrigationTask::init(int period){
  Task::init(period);
  state = IDLE;
  intensity = 0;
  
}

void IrrigationTask::manageLed(){
	led->setIntensity(map(intensity, 0, P_MAX, 0, 255));
}

  
void IrrigationTask::tick(){
	//Serial.println("tick");
	switch(state) {
    case IDLE:
		if(IrrigationStatus::getInstance()->isOn()){
			intensity = IrrigationStatus::getInstance()->getIntensity();
			// Open the Valve
			state = WORKING;
			servo->move((map(intensity, 0, P_MAX, MIN_OPENING, MAX_OPENING)));
		}     
    break;
    case WORKING:
		if(!IrrigationStatus::getInstance()->isOn()){
			// Close the Valve
			state = IDLE;
			servo->close();
			intensity = 0;
		}else {
			// If irrigation mode is changed, set the valve
			if(intensity != IrrigationStatus::getInstance()->getIntensity()){
				intensity = IrrigationStatus::getInstance()->getIntensity();
				servo->move((map(intensity, 0, P_MAX, MIN_OPENING, MAX_OPENING)));
			}
		}
    break;      
	}
	manageLed();
}
