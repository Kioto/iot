#ifndef __IRRIGATIONTASK__
#define __IRRIGATIONTASK__

#include "Task.h"
#include "Configuration.h"
#include "IrrigationStatus.h"
#include "Arduino.h"
#include "LedExt.h"
#include "ServoMotor.h"



class IrrigationTask: public Task {




public:

  IrrigationTask(LightExt* led, Motor* servo);  
  void init(int period);  
  void tick();
	

private:
	enum { IDLE, WORKING } state;
	int intensity;
	void manageLed();
	LightExt* led;
	Motor* servo;

};

#endif
