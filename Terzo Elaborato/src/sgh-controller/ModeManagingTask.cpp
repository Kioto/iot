#include "ModeManagingTask.h"
#include "Arduino.h"
#include "MsgServiceSerial.h"
#include <string.h>

ModeManagingTask::ModeManagingTask(Light* led1, Light* ledM, SonarSensor* snr, MsgServiceBT* btChannel){
  this->led1 = led1;
  this->ledM = ledM;
  this->snr = snr;
  this->btChannel = btChannel;
  state = AUTOMATIC;
  led1->switchOn();
  ledM->switchOff();
}
  
void ModeManagingTask::init(int period){
  Task::init(period);
  MsgService.init();
}
  
void ModeManagingTask::tick(){
  
  switch(state) {
    case AUTOMATIC:
	
      //check if there's a close bluetooth device that sent a message
      if(snr->getDistance() < DIST && btChannel->isMsgAvailable()) {
        Msg* msg = btChannel->receiveMsg();
        if(msg != NULL) {
          //the message 'start' is the message that start bluetooth communication for the MANUAL MODE
          if (msg->getContent() == "START") {
            state = MANUAL;
            MsgService.sendMsg("MANUAL");
            led1->switchOff();
            ledM->switchOn();
          }
        }
      } else {
        /* check if there's an automatic request for switchOn/Off the irrigation */
		
        if(MsgService.isMsgAvailable()) {
		//Serial.print("Ho ricevuto  ");
          Msg* msg = MsgService.receiveMsg();
		  Serial.println(msg->getContent());
          if(msg != NULL) {
            this->requestToIrrigator(msg->getContent());
          }
        }
      }
      break;
    case MANUAL:
      if(btChannel->isMsgAvailable()) {
        String msg = btChannel->receiveMsg()->getContent();
        if(msg == "END") {
          state = AUTOMATIC;
          MsgService.sendMsg("AUTO");
          led1->switchOn();
          ledM->switchOff();
        } else {
          MsgService.sendMsg(msg);
          this->requestToIrrigator(msg);
        }
      }
      if(MsgService.isMsgAvailable()) {
        btChannel->sendMsg(*(MsgService.receiveMsg()));
      }
      break;      
  }
}

void ModeManagingTask::requestToIrrigator(String msg) {
  if(msg == "OFF") {
    IrrigationStatus::getInstance()->switchOff();
  } else {
    if(msg == "MIN") {        
      IrrigationStatus::getInstance()->switchOn();
      IrrigationStatus::getInstance()->setIntensity(P_MIN);
    } else {
      if(msg == "MED") {
        IrrigationStatus::getInstance()->switchOn();
        IrrigationStatus::getInstance()->setIntensity(P_MED); 
      } else {
        if(msg == "MAX") {
          IrrigationStatus::getInstance()->switchOn();
          IrrigationStatus::getInstance()->setIntensity(P_MAX);
        }
      }
    }
  }
}
