#ifndef __MODEMANAGINGTASK__
#define __MODEMANAGINGTASK__

#include "Task.h"
#include "Configuration.h"
#include "Led.h"
#include "SonarSensor.h"
#include "IrrigationStatus.h"
#include "MsgServiceBT.h"


#define AUTOMATIC 1
#define MANUAL 2

class ModeManagingTask: public Task {

  Light* led1;
  Light* ledM;
  SonarSensor* snr;
  MsgServiceBT* btChannel;
  int state;
  unsigned long timer;

public:
  ModeManagingTask(Light* led1, Light* ledM, SonarSensor* snr, MsgServiceBT* btChannel);  
  void init(int period);  
  void tick();

private:
  void requestToIrrigator(String msg);
};

#endif
