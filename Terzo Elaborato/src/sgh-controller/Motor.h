#ifndef __MOTOR__
#define __MOTOR__

class Motor {
public:
  virtual void move(int angle) = 0;
  virtual void close() = 0;
};

#endif

