#include "Scheduler.h"
#include "Arduino.h"
#include "Configuration.h"

void Scheduler::init(int basePeriod){
  this->basePeriod = basePeriod;
  nTasks = 0;
}

bool Scheduler::addTask(Task* task){
  if (nTasks < MAX_TASKS-1){
    taskList[nTasks] = task;
    nTasks++;
    return true;
  } else {
    return false; 
  }
}

/*
 * Enter sleep mode, with Timer 1 active
 */

void Scheduler::sleep(){

}

void Scheduler::run(){
	delayMicroseconds(basePeriod * MICROSEC_IN_MILLISEC);
  /** The program will continue from here. **/ 
  for (int i = 0; i < nTasks; i++){
    if (taskList[i]->updateAndCheckTime(basePeriod)){
      taskList[i]->tick();
    }    
  }
}
