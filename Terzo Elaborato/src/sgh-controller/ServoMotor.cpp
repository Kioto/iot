#include "Led.h"
#include "Arduino.h"
#include "ServoMotor.h"
#include "Configuration.h"

ServoMotor::ServoMotor(int pin){
	this->pin = pin;
	servo.attach(SERVO_MOTOR_PIN);
	close();
}

void ServoMotor::move(int angle){
	servo.write(angle);
}

void ServoMotor::close(){
	servo.write(MIN_OPENING);
}

