#ifndef __SERVOMOTOR__
#define __SERVOMOTOR__

#include "Motor.h"
#include <ServoTimer2.h>


class ServoMotor: public Motor { 
public:
  ServoMotor(int pin);
  void move(int angle);
  void close();
private:
  int pin;  
  ServoTimer2 servo;
};

#endif
