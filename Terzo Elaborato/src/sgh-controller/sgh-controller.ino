/* Third Assignment : Serra Smart.
   Made by: Rispoli Luca, Andrea Lavista
   Please do see the attached documentation for further informations.
*/
#include "Configuration.h"
#include "Scheduler.h"
#include "ServoMotor.h"
#include "IrrigationTask.h"
#include "ModeManagingTask.h"
#include "MsgServiceBT.h"
#include "SoftwareSerial.h"
#include "Msg.h"
#include "LedExt.h"

Scheduler sched;

void setup() {
  sched.init(100);
  Light* led1 = new Led(LED1_PIN);
  LightExt* led2 = new LedExt(LED2_PIN,0);
  Light* ledM = new Led(LEDM_PIN);
  Motor* servo = new ServoMotor(SERVO_MOTOR_PIN);
  SonarSensor* snr = new SonarSensor(SNR_ECHO_PIN, SNR_TRIG_PIN);
  MsgServiceBT* btChannel = new MsgServiceBT(BT_RX, BT_TX);
  btChannel->init();
  //configure the name of bluetooth device, "sgh-bt" 
  //btChannel->sendMsg(*(new Msg("AT")));
  //delay(1000);
  //btChannel->sendMsg(*(new Msg("AT+NAMEsgh-bt")));

  /* declaration and initialization of tasks */
  Task* irrigation = new IrrigationTask(led2,servo);
  irrigation->init(100);
  Task* modeManaging = new ModeManagingTask(led1, ledM, snr, btChannel);
  modeManaging->init(100);

  sched.addTask(irrigation);  
  sched.addTask(modeManaging);
}

void loop() {
  sched.run();
}
