#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <DHTesp.h>
#define POT_PIN A0

/* wifi network name */
char* ssidName = "Vodafone-30088265";
//char* ssidName = "Si Sole";
//char* ssidName = "HUAWEI_P8_lite";
/* WPA2 PSK password */
char* pwd = "zuppadavid123";
//char* pwd = "1234567890";
//char* pwd = "b2724397ed23";
/* service IP address */ 
char* address = "http://9a924b8a.ngrok.io";

//DHTesp dht;

void setup() { 
  Serial.begin(115200);           
  //int dhtPin = D1;                     
  //dht.setup(dhtPin, DHTesp::DHT11);
  pinMode(POT_PIN, INPUT);
  WiFi.begin(ssidName, pwd);
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {  
    delay(500);
    Serial.print(".");
  } 
  Serial.println("Connected: \n local IP: "+WiFi.localIP());
}

int sendData(String address, float value, String label){  
   HTTPClient http;    
   http.begin(address + "/api/humidity");      
   http.addHeader("Content-Type", "application/json");     
   String msg = 
        String("{ \"value\": ") + String(value) + 
    ", \"label\": \"" + label +"\" }";
	Serial.println(msg);
   int retCode = http.POST(msg);   
   http.end();  
      
   String payload = http.getString();  
   Serial.println(payload);      
   return retCode;
}
   
void loop() { 
 if (WiFi.status()== WL_CONNECTED){   
   //TempAndHumidity lastValues = dht.getTempAndHumidity();
   //Serial.println("Humidity: " + String(lastValues.humidity,0));
   //int code = sendData(address, lastValues.humidity, "Humidity");
   int humidity = map(analogRead(POT_PIN), 0, 1023, 0, 100);
   Serial.println("Humidity: " + String(humidity));
   int code = sendData(address, humidity, "Humidity");
   /* log result */
   if (code == 200){
     Serial.println("ok");   
   } else {
     Serial.println("error");
   }
 } else { 
   Serial.println("Error in WiFi connection");   
 }
 
 delay(2000);  
}
