package com.example.andre.smartgreenhouse;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;

import com.example.andre.smartgreenhouse.btlib.BluetoothChannel;
import com.example.andre.smartgreenhouse.btlib.ConnectToBluetoothServerTask;
import com.example.andre.smartgreenhouse.btlib.ConnectionTask;
import com.example.andre.smartgreenhouse.btlib.RealBluetoothChannel;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    public static final String START_MESSAGE = "START";
    public static final String OFF_MESSAGE = "OFF";
    public static final String MIN_MESSAGE = "MIN";
    public static final String MED_MESSAGE = "MED";
    public static final String MAX_MESSAGE = "MAX";
    public static final String END_MESSAGE = "END";
    public static final int ENABLE_BT_REQUEST = 1;
    public static final String BT_SERVER_NAME = "sgh-bt";
    public static final String BT_SERVER_UUID = "00001101-0000-1000-8000-00805F9B34FB";
    private BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothDevice smartGreenHouseDevice = null;
    private BluetoothChannel btChannel;

    private final BroadcastReceiver br = new BroadcastReceiver(){
        @Override
        public void onReceive (Context context, Intent intent){
            BluetoothDevice device;
            if(BluetoothDevice.ACTION_FOUND.equals(intent.getAction())){
                device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if(device.getName().contains(BT_SERVER_NAME)) {
                    //smart green house's bluetooth device found
                    smartGreenHouseDevice = device;
                    btAdapter.cancelDiscovery();
                    unregisterReceiver(this);
                    connectToBluetoothDevice();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //init GUI elements
        final Button connectButton = (Button) findViewById(R.id.connect);
        final Button disconnectButton = (Button) findViewById(R.id.disconnect);
        final Switch irrigatorSwitch = (Switch) findViewById(R.id.irrigatorSwitch);
        final SeekBar irrigatorLevel = (SeekBar) findViewById(R.id.level);
        final EditText humidityText = (EditText) findViewById(R.id.humidityValue);

        disconnectButton.setEnabled(false);
        irrigatorSwitch.setEnabled(false);
        irrigatorLevel.setEnabled(false);

        if (btAdapter != null && !btAdapter.isEnabled()) {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), ENABLE_BT_REQUEST);
        }

        //connect to bluetooth device
        connectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //check if bluetooth is working
                if (!btAdapter.isEnabled()) {
                    startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), ENABLE_BT_REQUEST);
                } else {
                    //check if smart_green_house's bluetooth device is already paired
                    Set<BluetoothDevice> pairedList = btAdapter.getBondedDevices();
                    if (pairedList.size() > 0) {
                        for (BluetoothDevice device : pairedList) {
                            if (device.getName().contains(BT_SERVER_NAME)) {
                                smartGreenHouseDevice = device;
                            }
                        }
                    }
                    //try to discovery smart_green_house's bluetooth device, because it isn't already paired
                    if (smartGreenHouseDevice == null) {
                        registerReceiver(br, new IntentFilter(BluetoothDevice.ACTION_FOUND));
                        btAdapter.startDiscovery();
                    } else {
                        connectToBluetoothDevice();
                    }
                }
            }
        });

        //connect to bluetooth device
        disconnectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendMessage(END_MESSAGE);
                btChannel.close();
                connectButton.setEnabled(true);
                disconnectButton.setEnabled(false);
                irrigatorSwitch.setEnabled(false);
                irrigatorLevel.setEnabled(false);
            }
        });

        //switch on/off irrigation
        irrigatorSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sendMessage(getMsgFromLevel(irrigatorLevel.getProgress()));
                } else {
                    sendMessage(OFF_MESSAGE);
                }
            }
        });

        //change irrigation level
        irrigatorLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (irrigatorSwitch.isChecked()) {
                    sendMessage(getMsgFromLevel(irrigatorLevel.getProgress()));
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sendMessage(END_MESSAGE);
        btChannel.close();
    }

    //send message via bluetooth
    private void sendMessage(String msg){
        this.btChannel.sendMessage(msg);
    }

    //Convert seekbar level into the message for arduino
    private String getMsgFromLevel(int level) {
        switch (level) {
            case 0:
                return MIN_MESSAGE;
            case 1:
                return MED_MESSAGE;
            case 2:
                return MAX_MESSAGE;
            default:
                return "";
        }
    }

    private void connectToBluetoothDevice() {
        //init GUI elements
        final Button connectButton = (Button) findViewById(R.id.connect);
        final Button disconnectButton = (Button) findViewById(R.id.disconnect);
        final Switch irrigatorSwitch = (Switch) findViewById(R.id.irrigatorSwitch);
        final SeekBar irrigatorLevel = (SeekBar) findViewById(R.id.level);
        final EditText humidityText = (EditText) findViewById(R.id.humidityValue);
        final BluetoothSocket btSocket;

        AsyncTask<Void, Void, Integer> execute = new ConnectToBluetoothServerTask(smartGreenHouseDevice, UUID.fromString(BT_SERVER_UUID), new ConnectionTask.EventListener() {
            @Override
            public void onConnectionActive(final BluetoothChannel channel) {
                btChannel = channel;

                sendMessage(START_MESSAGE);
                connectButton.setEnabled(false);
                disconnectButton.setEnabled(true);
                irrigatorSwitch.setEnabled(true);
                irrigatorLevel.setEnabled(true);

                btChannel.registerListener(new RealBluetoothChannel.Listener() {
                    @Override
                    public void onMessageReceived(String receivedMessage) {
                        humidityText.setText(receivedMessage);
                    }

                    @Override
                    public void onMessageSent(String sentMessage) {
                    }
                });
            }

            @Override
            public void onConnectionCanceled() {
                btChannel.close();
            }
        }).execute();
    }
}
