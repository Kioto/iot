import common.CommChannel;
import common.SerialCommChannel;
import communication.ControllerReceiver;
import communication.DataService;
import io.vertx.core.Vertx;
import tasks.IrrigationControl;

public class Main {

    private static int RATE = 9600;
    private static String PORT = "COM10";

    public static void main(String[] args) throws Exception {
        Vertx vertx = Vertx.vertx();
        CommChannel controller = new SerialCommChannel(PORT, RATE);
        IrrigationControl irrigation = new IrrigationControl();
        irrigation.init(controller);
        irrigation.start();
        DataService service = new DataService(800, irrigation,irrigation.getLog());
        vertx.deployVerticle(service);
        ControllerReceiver receiver = new ControllerReceiver(controller, irrigation);
        receiver.start();
    }

}
