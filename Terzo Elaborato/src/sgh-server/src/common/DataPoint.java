package common;

public class DataPoint {
	private double value;
	private long time;
	private String label;
	
	public DataPoint(double value, long time2, String label) {
		this.value = value;
		this.time = time2;
		this.label = label;
	}
	
	public double getValue() {
		return value;
	}
	
	public long getTime() {
		return time;
	}
	
	public String getLabel() {
		return label;
	}
}
