package common;

/*
 * All the capacity of irrigation available 
 */
public enum Irrigation {
    Pmin("MIN"), Pmed("MED"), Pmax("MAX"), Off("OFF");

    private String value;

    Irrigation(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
