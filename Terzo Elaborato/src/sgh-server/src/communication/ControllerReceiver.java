package communication;

import common.*;
import messages.AutoActivation;
import messages.ManualActivation;
import messages.MsgEvent;
import messages.StartIrrigation;
import messages.StopIrrigation;

/*
 * Listener for the controller, when a message is received, an event is created for the corresponding entity.
 */
public class ControllerReceiver extends BasicController {
    private CommChannel controller;
    private ReactiveAgent irrigationControl;

    public ControllerReceiver(CommChannel controller, ReactiveAgent irrigationControl) {
        this.controller = controller;
        this.irrigationControl = irrigationControl;
    }

    @Override
    public void run() {
        while (true) {
            try {
                String msg = this.controller.receiveMsg();
                if (msg.contains("MANUAL")) {
                    MsgEvent ev = new MsgEvent(new ManualActivation());
                    irrigationControl.notifyEvent(ev);
                } else if (msg.contains("AUTO")) {
                    MsgEvent ev = new MsgEvent(new AutoActivation());
                    irrigationControl.notifyEvent(ev);
                } else if (msg.contains("MAX")) {
                    // Controller opened irrigation with maximum intensity, notice irrigation task
                    MsgEvent ev = new MsgEvent(new StartIrrigation(Irrigation.Pmax));
                    irrigationControl.notifyEvent(ev);
                }else if (msg.contains("MED")) {
                    // Controller opened irrigation with medium intensity, notice irrigation task
                    MsgEvent ev = new MsgEvent(new StartIrrigation(Irrigation.Pmed));
                    irrigationControl.notifyEvent(ev);
                }else if (msg.contains("MIN")) {
                    // Controller opened irrigation with minimum intensity, notice irrigation task
                    MsgEvent ev = new MsgEvent(new StartIrrigation(Irrigation.Pmin));
                    irrigationControl.notifyEvent(ev);
                }
                else if (msg.contains("OFF")) {
                    // Controller closed irrigation, notice irrigation task
                    MsgEvent ev = new MsgEvent(new StopIrrigation());
                    irrigationControl.notifyEvent(ev);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

}
