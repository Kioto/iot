package communication;

import io.vertx.core.AbstractVerticle;
import common.*;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import messages.HumidityUpdate;
import messages.MsgEvent;
import saves.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/*
 * Data Service as a vertx event-loop 
 * USAGE POST: curl -X POST http://be1047e1.ngrok.io/api/data
 * USAGE GET:  curl -X GET http://be1047e1.ngrok.io/api/data
 * 
 */
public class DataService extends AbstractVerticle {

    private int port;
    private ReactiveAgent irrigationControl;
    Log logger;

    public DataService(int port, ReactiveAgent irrigationControl, Log logger) {
        this.port = port;
        this.irrigationControl = irrigationControl;
        this.logger = logger;
    }

    @Override
    public void start() {
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.post("/api/humidity").handler(this::handleNewHumidityReading);
        router.get("/api/humidity").handler(this::handleGetHumidity);
        router.get("/api/status").handler(this::handleGetStatus);
        router.get("/api/history").handler(this::handleGetHistory);
        router.get("/api/signals").handler(this::handleGetSignals);
        vertx.createHttpServer().requestHandler(router::accept).listen(port);

        log("Service ready.");
    }

    // Handle humidity readings from controller
    private void handleNewHumidityReading(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        JsonObject res = routingContext.getBodyAsJson();
        if (res == null) {
            sendError(400, response);
        } else {

            float value = res.getFloat("value");
            MsgEvent ev = new MsgEvent(new HumidityUpdate(value));
            irrigationControl.notifyEvent(ev);
            log("New Reading - Value: " + value + " on " + new Date(System.currentTimeMillis()));

        }
    }

    // Handle humidity requests from Clients
    private void handleGetHumidity(RoutingContext routingContext) {
        JsonArray arr = new JsonArray();
        ArrayList<Pair<Float, Date>> humidityHistory = logger.getHumidityHistory();
        for (Pair<Float, Date> p : humidityHistory) {
            JsonObject data = new JsonObject();
            data.put("time", p.getElement1().toString());
            data.put("value (%)", p.getElement0().floatValue());
            data.put("label", "Humidity Reading");
            arr.add(data);
        }

        routingContext.response().putHeader("content-type", "application/json")
                .putHeader("Access-Control-Allow-Origin", "*").end(arr.toString());
    }

    // Handle history requests from Clients
    private void handleGetHistory(RoutingContext routingContext) {
        JsonArray arr = new JsonArray();
        ArrayList<Pair<Date, Integer>> irrigationHistory = logger.getIrrigationHistory();
        for (Pair<Date, Integer> p : irrigationHistory) {
            JsonObject data = new JsonObject();
            data.put("duration (MS)", p.getElement1().toString());
            data.put("date", p.getElement0().toString());
            arr.add(data);
        }

        routingContext.response().putHeader("content-type", "application/json")
                .putHeader("Access-Control-Allow-Origin", "*").end(arr.toString());
    }

    // Handle status requests from Clients
    private void handleGetStatus(RoutingContext routingContext) {
        JsonObject data = new JsonObject();
        data.put("status", logger.getStatus().getIntensity().toString());
        routingContext.response().putHeader("content-type", "application/json")
                .putHeader("Access-Control-Allow-Origin", "*").end(data.toString());

    }

    // Handle signals requests from Clients
    private void handleGetSignals(RoutingContext routingContext) {
        JsonArray arr = new JsonArray();
        List<Date> signals = logger.getSignals();
        for (Date p : signals) {
            JsonObject data = new JsonObject();
            data.put("date", p.toString());
            data.put("label", "Irrigation Time Exceeded");
            arr.add(data);
        }

        routingContext.response().putHeader("content-type", "application/json")
                .putHeader("Access-Control-Allow-Origin", "*").end(arr.toString());

    }

    private void sendError(int statusCode, HttpServerResponse response) {
        response.setStatusCode(statusCode).end();
    }

    private void log(String msg) {
        System.out.println("[DATA SERVICE] " + msg);
    }

}