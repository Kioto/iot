package messages;

public class HumidityUpdate implements Msg {
	
	private float humidity;
	
	public HumidityUpdate(float humidity){
		this.humidity = humidity;
	}
	
	public float getHumidity(){
		return humidity;
	}
	
}
