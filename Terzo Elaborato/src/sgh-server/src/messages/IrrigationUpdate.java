package messages;

import common.Irrigation;

public class IrrigationUpdate implements Msg {
	
	private Irrigation irrigationLevel;
	
	public IrrigationUpdate(Irrigation irrigationLevel){
		this.irrigationLevel = irrigationLevel;
	}
	
	public Irrigation getLevel(){
		return this.irrigationLevel;
	}
	
}
