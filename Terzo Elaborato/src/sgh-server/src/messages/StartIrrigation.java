package messages;

import common.Irrigation;

public class StartIrrigation implements Msg {

    Irrigation level;

    public StartIrrigation(Irrigation IrrigationLevel) {
        this.level = IrrigationLevel;
    }

    public Irrigation getLevel() {

        return level;
    }

}
