package saves;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import common.Irrigation;
import common.Pair;

public interface Log {
	
	void logHumidity(float humidity);
	
	void logIrrigationOpening(Irrigation intensity);
	
	void logIrrigationClosing();
	
	void logSignal();
	
	ArrayList<Pair<Float, Date>> getHumidityHistory();
	
	ArrayList<Pair<Date, Integer>> getIrrigationHistory();
	
	List<Date> getSignals();
	
	Status getStatus();

}
