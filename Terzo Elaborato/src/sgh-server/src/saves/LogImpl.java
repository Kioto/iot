package saves;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import common.Irrigation;
import common.Pair;

public class LogImpl implements Log {

        private ArrayList<Pair<Float, Date>> humidityHistory = new ArrayList<Pair<Float,Date>>();
	private ArrayList<Pair<Date, Integer>> irrigationHistory = new ArrayList<Pair<Date,Integer>>();
	private Status status;
	private List<Date> signals = new ArrayList<>();
	private static int MAX_BUFFER_SIZE = 10;
	
	public LogImpl(Irrigation intensity) {
        super();
        this.status = new StatusImpl(intensity);
    }

    @Override
	public void logHumidity(float humidity) {
	        if(humidityHistory.size() >= MAX_BUFFER_SIZE) {
	            humidityHistory.remove(0);
	        }
		humidityHistory.add((new Pair<Float,Date>(humidity,new Date())));
	}

	@Override
	public void logIrrigationOpening(Irrigation intensity) {
		status.switchIrrigationOn(intensity);
	}

	@Override
	public void logIrrigationClosing() {
		long duration = new Date().getTime() - this.status.getPowerOnDate().getTime();
		if(irrigationHistory.size() >= MAX_BUFFER_SIZE) {
		    irrigationHistory.remove(0);
		}
		irrigationHistory.add(new Pair<Date,Integer>(status.getPowerOnDate(), (int) duration));
		this.status.switchIrrigationOff();
	}

	@Override
	public void logSignal() {
		this.signals.add(new Date());
	}

	@Override
	public ArrayList<Pair<Float, Date>> getHumidityHistory() {
		return this.humidityHistory;
	}

	@Override
	public ArrayList<Pair<Date, Integer>> getIrrigationHistory() {
		return this.irrigationHistory;
	}

	@Override
	public Status getStatus() {
		return this.status;
	}

	@Override
	public List<Date> getSignals() {
		return this.signals;
	}

	@Override
	public String toString() {
		return "LogImpl [humidityHistory=" + humidityHistory + ", irrigationHistory=" + irrigationHistory + ", status="
				+ status + "]";
	}
	

}
