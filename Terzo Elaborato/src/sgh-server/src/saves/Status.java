package saves;

import java.util.Date;

import common.Irrigation;

public interface Status {
	
	void switchIrrigationOn(Irrigation intensity);
	
	void switchIrrigationOff();
	
	boolean isIrrigationOn();
	
	Irrigation getIntensity();
	
	Date getPowerOnDate();
}
