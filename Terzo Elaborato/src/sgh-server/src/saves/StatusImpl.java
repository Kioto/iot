package saves;

import java.util.Date;

import common.Irrigation;

public class StatusImpl implements Status {

    private boolean irrigationOn;
    private Date irrigationPowerOnDate;
    private Irrigation intensity;

    public StatusImpl(Irrigation intensity) {
        this.intensity = intensity;
    }

    @Override
    public synchronized void switchIrrigationOn(Irrigation intensity) {
        this.irrigationOn = true;
        this.irrigationPowerOnDate = new Date();
        this.intensity = intensity;
    }

    @Override
    public synchronized void switchIrrigationOff() {
        this.irrigationOn = false;
        this.intensity = Irrigation.Off;
    }

    @Override
    public synchronized boolean isIrrigationOn() {
        return this.irrigationOn;
    }

    @Override
    public synchronized Irrigation getIntensity() {
        return this.intensity;
    }

    @Override
    public synchronized String toString() {
        String result = "Stato irrigazione: ";
        result += (this.irrigationOn) ? "attiva, portata " + this.intensity.getValue() : "spenta";
        return result;
    }

    @Override
    public synchronized Date getPowerOnDate() {
        return this.irrigationPowerOnDate;
    }

}
