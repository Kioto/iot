package saves;

import java.util.Random;

import common.Irrigation;

public class Test {

    public static void main(String[] args) throws InterruptedException {
        Log logger = new LogImpl(Irrigation.Off);
        for (int i = 0; i < 10; i++) {
            logger.logHumidity(i);
        }
        for (int i = 0; i < 10; i++) {
            logger.logIrrigationOpening(Irrigation.Pmin);
            int d = new Random().nextInt(300);
            Thread.sleep(d);
            if (i % 3 == 0) {
                logger.logSignal();
            }
            logger.logIrrigationClosing();
        }
        System.out.println(logger.getHumidityHistory());
        System.out.println(logger.getIrrigationHistory());
        System.out.println(logger.getSignals());
        System.out.println(logger.getStatus());
        logger.logIrrigationOpening(Irrigation.Pmed);
        System.out.println(logger.getStatus());
    }

}
