package tasks;

import java.time.Instant;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import common.*;
import messages.AutoActivation;
import messages.HumidityUpdate;
import messages.ManualActivation;
import messages.Msg;
import messages.MsgEvent;

import messages.StartIrrigation;
import messages.StopIrrigation;

import saves.Log;
import saves.LogImpl;

// Irrigation Control Task, contains all irrigation logic used during AutoMode

public class IrrigationControl extends ReactiveAgent {
    private static int U_MIN = 30; // Minimum irrigation treshold
    private static int P_MIN = 20; // Medium irrigation threshold
    private static int P_MED = 10; // Maximum irrigation threshold
    private static int DELTA_U = 5; // Delta U to deactivate irrigation
    private static int T_MAX = 5000; // Maximum time to irrigate (ms)

    private boolean working = false;
    private ObservableTimer timer;
    // Watchdog Timer to deactivate irrigation
    private ScheduledFuture<?> currentTimer;
    private CommChannel controller;
    private boolean autoMode = true;
    private Irrigation currentLevel = Irrigation.Off;
    private Log log ;

    public void init(CommChannel controller) {
        this.controller = controller;
        this.timer = new ObservableTimer();
        timer.addObserver(this);
        log = new LogImpl(currentLevel);
    }

    public Log getLog() {
        return this.log;
    }

    @Override
    protected void processEvent(Event ev) {
    	if (ev instanceof MsgEvent) {
            Msg msg = ((MsgEvent) ev).getMsg();
            if (msg instanceof HumidityUpdate) {
                float humidity = ((HumidityUpdate) msg).getHumidity();
                this.log.logHumidity(humidity);
                if (autoMode) {
                    if (!withinThreshold(humidity, U_MIN) && !working) {
                        // Turn on Irrigation
                        currentLevel = computeIrrigation(humidity);
                        controller.sendMsg(currentLevel.getValue());
                        log("Irrigation is turned ON , Level is: " + currentLevel.getValue());
                        currentTimer = timer.scheduleTick(T_MAX); // Start Watch Dog Timer for Irrigation
                        working = true;
                        this.log.logIrrigationOpening(currentLevel);
                    } else if (withinThreshold(humidity, U_MIN + DELTA_U) && working) {
                        controller.sendMsg(Irrigation.Off.getValue());
                        log("Irrigation is turned OFF : No longer Needed");
                        log.logIrrigationClosing();
                        working = false;
                        currentTimer.cancel(true);
                    }
                } else {
                    // Manual Mode active, Send humidity update to controller.
                    controller.sendMsg(Float.toString(humidity));
                }
            } else if (msg instanceof ManualActivation) {
                autoMode = false;
            } else if (msg instanceof AutoActivation) {
                autoMode = true;
            } else if (msg instanceof StartIrrigation) {
                this.log.logIrrigationOpening(((StartIrrigation) msg).getLevel());

            } else if (msg instanceof StopIrrigation) {
                this.log.logIrrigationClosing();

            }
        } else if (ev instanceof Tick && autoMode) {
            // Watch dog timer expired, turn off irrigation
            controller.sendMsg(Irrigation.Off.getValue());
            log("Irrigation is turned OFF : Timer Expired");
            working = false;
            this.log.logSignal();
            this.log.logIrrigationClosing();
        }

    }

    private boolean withinThreshold(float humidity, int threshold) {
        return humidity > threshold;
    }

    private Irrigation computeIrrigation(float humidity) {
        if (humidity >= P_MIN) {
            return Irrigation.Pmin;
        } else if (humidity >= P_MED) {
            return Irrigation.Pmed;
        } else {
            return Irrigation.Pmax;
        }
    }

    private void log(String msg) {
        System.out.println("[IRRIGATION CONTROL] " + msg);
    }

    public boolean getStatus() {
        return working;
    }
}
